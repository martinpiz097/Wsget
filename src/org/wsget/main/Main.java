package org.wsget.main;

import org.wsget.net.WebServiceGetter;
import org.wsget.sys.ConsoleColor;

import static org.wsget.sys.ConsoleColor.ANSI_GREEN;

public class Main {
    public static void main(String[] args) {
        try {
            WebServiceGetter getter;

            switch (args.length) {
                case 0:
                    System.out.println("Url is not defined");
                    break;

                case 1:
                    String url = args[0];
                    if (!url.startsWith("http://"))
                        url = new
                                StringBuilder()
                                .append("http://")
                                .append(url).toString();
                    getter = new WebServiceGetter(url);
                    getter.printResponse(ANSI_GREEN);
                case 4:
                    getter = new WebServiceGetter(args[0], args[1],
                            Integer.parseInt(args[2]), args[3]);
                    getter.printResponse(ANSI_GREEN);
                    default:
                        break;
            }
        } catch (Exception e) {
            String exMsg = e.getMessage();
            if (exMsg != null) {
                System.err.println("ExceptionMessage: "+exMsg);
            }
            Throwable cause = e.getCause();
            if (cause != null)
                System.err.println("Cause: "+cause.toString());
        }
    }
}
