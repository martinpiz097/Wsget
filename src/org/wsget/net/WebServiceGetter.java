package org.wsget.net;

import org.wsget.sys.ConsoleColor;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceGetter {
    private URL url;
    private FileOutputStream stdout;

    private static final byte ENDLINE = '\n';

    public WebServiceGetter(URL url) {
        this.url = url;
        try {
            stdout = getStdout();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public WebServiceGetter(String url) throws MalformedURLException {
        this(new URL(url));
    }

    public WebServiceGetter
            (String protocol, String host, int port, String file)
            throws MalformedURLException {
        this(new URL(protocol, host, port, file));
    }

    private FileDescriptor getFdStdout()
            throws IllegalAccessException, NoSuchFieldException {
        Field fieldFd = FileDescriptor.class.getDeclaredField("fd");
        fieldFd.setAccessible(true);
        FileDescriptor fdIn = new FileDescriptor();
        fieldFd.setInt(fdIn, 1);
        return fdIn;
    }

    private FileOutputStream getStdout()
            throws NoSuchFieldException, IllegalAccessException {
        return new FileOutputStream(getFdStdout());
    }

    private InputStream getUrlStream() throws IOException {
        return url.openStream();
    }

    public String getResponse() throws IOException {
        InputStream wsStream = getUrlStream();
        StringBuilder sbService = new StringBuilder();

        byte last = 0;
        while (wsStream.available() > 0) {
            last = (byte) wsStream.read();
            sbService.append(last);
        }
        if (last != ENDLINE)
            sbService.append(ENDLINE);
        return sbService.toString();
    }

    public String getResponse(String color) throws IOException {
        InputStream wsStream = getUrlStream();
        StringBuilder sbService = new StringBuilder();

        sbService.append(color);
        byte last = 0;
        while (wsStream.available() > 0) {
            last = (byte) wsStream.read();
            sbService.append(last);
        }
        if (last != ENDLINE)
            sbService.append(ENDLINE);
        sbService.append(ConsoleColor.RESET);
        return sbService.toString();
    }

    public void printResponse()
            throws IOException {
        InputStream wsStream = getUrlStream();
        byte last = 0;
        while (wsStream.available() > 0) {
            last = (byte) wsStream.read();
            stdout.write(last);
        }
        if (last != ENDLINE)
            stdout.write(ENDLINE);
    }

    public void printResponse(String color) throws IOException {
        InputStream wsStream = getUrlStream();
        stdout.write(color.getBytes());
        byte last = 0;
        while (wsStream.available() > 0) {
            last = (byte) wsStream.read();
            stdout.write(last);
        }
        stdout.write(ConsoleColor.RESET.getBytes());
        if (last != ENDLINE)
            stdout.write(ENDLINE);
    }

}
